import java.util.Scanner;

public class Next_Line {
    Scanner input = new Scanner(System.in); //创建输入对象

        String shuNext = input.next();
        /**
         * next()方法在读取内容时，会过滤掉有效字符前面的无效字符，
         * 对输入有效字符之前遇到的空格键、Tab键或Enter键等结束符，next()方法会自动将其过滤掉；
         * 只有在读取到有效字符之后，next()方法才将其后的空格键、Tab键或Enter键等视为结束符；
         * 所以next()方法不能得到带空格的字符串。
         */

        String shuNextline = input.nextLine();

        /**
         * nextLine()方法字面上有扫描一整行的意思，它的结束符只能是Enter键，
         * 即nextLine()方法返回的是Enter键之前没有被读取的所有字符，
         * 它是可以得到带空格的字符串的.
         */

}
