public class LeiXingZhuanHuan {

    /**
     * 强制类型转换
     */
    double num = 3.14;
    int num1 = (int)num;

    double num2 = 5/2;  // int除以int得到int（2），再转成double（2.0）
    double num3 = 5/2.0; // int除以double得到double（2.5），再转成double（2.5）

    /**
     * short->int->long->float->double
     */
}
