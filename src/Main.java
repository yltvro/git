
/**
 * @ author:YeeLin-learn
 * CMD : javadoc xxx.java -->生成文档
 */
public class Main {
    public static void main(String[] args) {

        /**类的运用*/
        System.out.println("This my test1!");
        Lei Jtest = new Lei();
        int sum = Jtest.add(3,6);
        int count = Jtest.shen(5,7);
        System.out.println(sum+"\t"+count);

        /**Next()& Nextline()区别：*/
        Next_Line shurutest = new Next_Line();
        System.out.println(shurutest.shuNext);
        System.out.println(shurutest.shuNextline);

        /**强制转换类型*/
        LeiXingZhuanHuan zhuanhuan = new LeiXingZhuanHuan();
        System.out.println(zhuanhuan.num1);
        System.out.println(zhuanhuan.num2+"\n"+zhuanhuan.num3);

        /** learn1*/
        Learn1 learn = new Learn1();
        System.out.println(learn.yunSuanFu());

    }
}
